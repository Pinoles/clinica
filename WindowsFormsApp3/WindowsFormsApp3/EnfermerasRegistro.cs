﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class EnfermerasRegistro : Form
    {
        public EnfermerasRegistro()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            conexion con = new conexion();
            string txtNombre = txtBxNombre.Text;
            string txtPrimerApellido = txtBxPrimerApellido.Text;
            string txtSegundoApellido = txtBxSegundoApellido.Text;
            string strCalendario = Calendario.SelectionRange.Start.ToShortDateString();

            string txtTelCel = txtBxTelCelular.Text;

            string comando = "insert into ENFERMERAS(NOMBRE,PRIMER_APELLIDO,SEGUNDO_APELLIDO" +
                ",FECHA_INGRESO,TEL_CELULAR)" +
                " values ('" + txtNombre + "','" + txtPrimerApellido + "','" + txtSegundoApellido + "','" +
                strCalendario + "','" + txtTelCel + "')";
            if (con.Comando(comando) > 0)
                MessageBox.Show("yeah");
            else
                MessageBox.Show("noyeah");
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EnfermerasRegistro_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is menu_normal);
            frm.Show();
        }
    }
}
