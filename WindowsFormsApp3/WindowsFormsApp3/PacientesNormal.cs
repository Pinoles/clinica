﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class PacientesNormal : Form
    {
        public PacientesNormal()
        {
            InitializeComponent();
        }

        private void PacientesNormal_Load(object sender, EventArgs e)
        {
            conexion conn = new conexion();
            dtGrdPacientes.DataSource = conn.Llenartabla("seleccionarPacientes");
        }
        
        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PacientesNormal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is menu_normal);
            frm.Show();
        }
    }
}
