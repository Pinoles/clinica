﻿namespace WindowsFormsApp3
{
    partial class menu_normal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPacientes = new System.Windows.Forms.Button();
            this.btnTratamiento = new System.Windows.Forms.Button();
            this.btnIngreso = new System.Windows.Forms.Button();
            this.btnDoctores = new System.Windows.Forms.Button();
            this.btnEnfermeras = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPacientes
            // 
            this.btnPacientes.Location = new System.Drawing.Point(344, 99);
            this.btnPacientes.Name = "btnPacientes";
            this.btnPacientes.Size = new System.Drawing.Size(90, 35);
            this.btnPacientes.TabIndex = 0;
            this.btnPacientes.Text = "pacientes";
            this.btnPacientes.UseVisualStyleBackColor = true;
            this.btnPacientes.Click += new System.EventHandler(this.btnPacientes_Click);
            // 
            // btnTratamiento
            // 
            this.btnTratamiento.Location = new System.Drawing.Point(344, 140);
            this.btnTratamiento.Name = "btnTratamiento";
            this.btnTratamiento.Size = new System.Drawing.Size(90, 35);
            this.btnTratamiento.TabIndex = 1;
            this.btnTratamiento.Text = "tratamiento";
            this.btnTratamiento.UseVisualStyleBackColor = true;
            this.btnTratamiento.Click += new System.EventHandler(this.btnTratamiento_Click);
            // 
            // btnIngreso
            // 
            this.btnIngreso.Location = new System.Drawing.Point(344, 181);
            this.btnIngreso.Name = "btnIngreso";
            this.btnIngreso.Size = new System.Drawing.Size(90, 35);
            this.btnIngreso.TabIndex = 2;
            this.btnIngreso.Text = "ingreso";
            this.btnIngreso.UseVisualStyleBackColor = true;
            this.btnIngreso.Click += new System.EventHandler(this.btnIngreso_Click);
            // 
            // btnDoctores
            // 
            this.btnDoctores.Location = new System.Drawing.Point(344, 222);
            this.btnDoctores.Name = "btnDoctores";
            this.btnDoctores.Size = new System.Drawing.Size(90, 35);
            this.btnDoctores.TabIndex = 3;
            this.btnDoctores.Text = "doctores";
            this.btnDoctores.UseVisualStyleBackColor = true;
            this.btnDoctores.Click += new System.EventHandler(this.btnDoctores_Click);
            // 
            // btnEnfermeras
            // 
            this.btnEnfermeras.Location = new System.Drawing.Point(344, 263);
            this.btnEnfermeras.Name = "btnEnfermeras";
            this.btnEnfermeras.Size = new System.Drawing.Size(90, 35);
            this.btnEnfermeras.TabIndex = 4;
            this.btnEnfermeras.Text = "enfermeras";
            this.btnEnfermeras.UseVisualStyleBackColor = true;
            this.btnEnfermeras.Click += new System.EventHandler(this.btnEnfermeras_Click);
            // 
            // menu_normal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnEnfermeras);
            this.Controls.Add(this.btnDoctores);
            this.Controls.Add(this.btnIngreso);
            this.Controls.Add(this.btnTratamiento);
            this.Controls.Add(this.btnPacientes);
            this.Name = "menu_normal";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPacientes;
        private System.Windows.Forms.Button btnTratamiento;
        private System.Windows.Forms.Button btnIngreso;
        private System.Windows.Forms.Button btnDoctores;
        private System.Windows.Forms.Button btnEnfermeras;
    }
}

