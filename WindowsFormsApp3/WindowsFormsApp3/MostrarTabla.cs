﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class MostrarTabla : Form
    {
        string strProcedimiento = null;
        int registrarTabla = 0;

        public MostrarTabla()
        {
            InitializeComponent();
        }

        public MostrarTabla(string c_strProcedimiento,int c_registrarTabla)
        {
            InitializeComponent();
            strProcedimiento = c_strProcedimiento;
            registrarTabla = c_registrarTabla;
        }

        private void PacientesNormal_Load(object sender, EventArgs e)
        {
            conexion conn = new conexion();
            dtGrdPacientes.DataSource = conn.Llenartabla(strProcedimiento);
        }
        
        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PacientesNormal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is menu_normal);
            frm.Show();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (registrarTabla == 1)
            {
                PacientesRegistro cambio = new PacientesRegistro();
                cambio.Show();
                this.Hide();
            }
            else if (registrarTabla == 2)
            {
                MostrarTabla cambio = new MostrarTabla("seleccionarEnfermeras", 5);
                cambio.Show();
                this.Hide();
            }
            else if (registrarTabla == 3)
            {
                MostrarTabla cambio = new MostrarTabla("seleccionarEnfermeras", 5);
                cambio.Show();
                this.Hide();
            }
            else if (registrarTabla == 4)
            {
                MostrarTabla cambio = new MostrarTabla("seleccionarEnfermeras", 5);
                cambio.Show();
                this.Hide();
            }
            else if (registrarTabla == 5)
            {
                EnfermerasRegistro cambio = new EnfermerasRegistro();
                cambio.Show();
                this.Hide();
            }
        }
    }
}
