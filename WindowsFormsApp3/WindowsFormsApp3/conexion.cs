﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OracleClient;

namespace WindowsFormsApp3
{
    class conexion
    { 
        static private OracleConnection conn = new OracleConnection();

        static conexion()
        {
            conn.ConnectionString = "Data Source=xe;User ID=UNIPOLI1;Password=UNIPOLI;Unicode=True;";
            
        }

        public int Comando(string strComando)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = strComando;
            cmd.CommandType = CommandType.Text;
            try
            {
                conn.Open();
                int res = cmd.ExecuteNonQuery();
                conn.Close();
                return res;
            }
            catch
            {
                return 0;
            }
           
        }

        public DataTable Llenartabla(string strProcedimiento)
        {
            conn.Open();
            OracleCommand comd = new OracleCommand(strProcedimiento, conn);
            comd.CommandType = CommandType.StoredProcedure;
            comd.Parameters.Add("registros", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comd;
            DataTable tabla = new DataTable();
            adapter.Fill(tabla);
            conn.Close();
            return tabla;
        }
    }
}
