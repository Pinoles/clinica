﻿namespace WindowsFormsApp3
{
    partial class PacientesRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBxNombre = new System.Windows.Forms.TextBox();
            this.txtBxPrimerApellido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBxSegundoApellido = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBxTelCasa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBxTelCelular = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBxFamiliar = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBxTelFamiliar = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Calendario = new System.Windows.Forms.MonthCalendar();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.txtBxAlergias = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // txtBxNombre
            // 
            this.txtBxNombre.Location = new System.Drawing.Point(226, 37);
            this.txtBxNombre.MaxLength = 20;
            this.txtBxNombre.Name = "txtBxNombre";
            this.txtBxNombre.Size = new System.Drawing.Size(206, 20);
            this.txtBxNombre.TabIndex = 1;
            // 
            // txtBxPrimerApellido
            // 
            this.txtBxPrimerApellido.Location = new System.Drawing.Point(226, 63);
            this.txtBxPrimerApellido.MaxLength = 20;
            this.txtBxPrimerApellido.Name = "txtBxPrimerApellido";
            this.txtBxPrimerApellido.Size = new System.Drawing.Size(206, 20);
            this.txtBxPrimerApellido.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Primer Apellido";
            // 
            // txtBxSegundoApellido
            // 
            this.txtBxSegundoApellido.Location = new System.Drawing.Point(226, 89);
            this.txtBxSegundoApellido.MaxLength = 20;
            this.txtBxSegundoApellido.Name = "txtBxSegundoApellido";
            this.txtBxSegundoApellido.Size = new System.Drawing.Size(206, 20);
            this.txtBxSegundoApellido.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(130, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Segundo Apellido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Fecha Nacimieto";
            // 
            // txtBxTelCasa
            // 
            this.txtBxTelCasa.Location = new System.Drawing.Point(227, 318);
            this.txtBxTelCasa.MaxLength = 7;
            this.txtBxTelCasa.Name = "txtBxTelCasa";
            this.txtBxTelCasa.Size = new System.Drawing.Size(206, 20);
            this.txtBxTelCasa.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(170, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Tel Casa";
            // 
            // txtBxTelCelular
            // 
            this.txtBxTelCelular.Location = new System.Drawing.Point(227, 344);
            this.txtBxTelCelular.MaxLength = 10;
            this.txtBxTelCelular.Name = "txtBxTelCelular";
            this.txtBxTelCelular.Size = new System.Drawing.Size(206, 20);
            this.txtBxTelCelular.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(162, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Tel Celular";
            // 
            // txtBxFamiliar
            // 
            this.txtBxFamiliar.Location = new System.Drawing.Point(227, 370);
            this.txtBxFamiliar.MaxLength = 20;
            this.txtBxFamiliar.Name = "txtBxFamiliar";
            this.txtBxFamiliar.Size = new System.Drawing.Size(206, 20);
            this.txtBxFamiliar.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 370);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Familiar Responsable";
            // 
            // txtBxTelFamiliar
            // 
            this.txtBxTelFamiliar.Location = new System.Drawing.Point(227, 396);
            this.txtBxTelFamiliar.MaxLength = 10;
            this.txtBxTelFamiliar.Name = "txtBxTelFamiliar";
            this.txtBxTelFamiliar.Size = new System.Drawing.Size(206, 20);
            this.txtBxTelFamiliar.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(115, 399);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Tel del Responsable";
            // 
            // Calendario
            // 
            this.Calendario.Location = new System.Drawing.Point(226, 116);
            this.Calendario.MaxSelectionCount = 1;
            this.Calendario.Name = "Calendario";
            this.Calendario.TabIndex = 15;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(227, 451);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 16;
            this.btnRegistrar.Text = "registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(309, 450);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(391, 449);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 18;
            this.btnVolver.Text = "volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // txtBxAlergias
            // 
            this.txtBxAlergias.Location = new System.Drawing.Point(226, 290);
            this.txtBxAlergias.MaxLength = 20;
            this.txtBxAlergias.Name = "txtBxAlergias";
            this.txtBxAlergias.Size = new System.Drawing.Size(206, 20);
            this.txtBxAlergias.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(175, 290);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Alergias";
            // 
            // PacientesRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 501);
            this.Controls.Add(this.txtBxAlergias);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.Calendario);
            this.Controls.Add(this.txtBxTelFamiliar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtBxFamiliar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBxTelCelular);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBxTelCasa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBxSegundoApellido);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBxPrimerApellido);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBxNombre);
            this.Controls.Add(this.label1);
            this.Name = "PacientesRegistro";
            this.Text = "PacientesRegistro";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PacientesRegistro_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBxNombre;
        private System.Windows.Forms.TextBox txtBxPrimerApellido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBxSegundoApellido;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBxTelCasa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBxTelCelular;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBxFamiliar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBxTelFamiliar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MonthCalendar Calendario;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.TextBox txtBxAlergias;
        private System.Windows.Forms.Label label9;
    }
}