﻿namespace WindowsFormsApp3
{
    partial class PacientesNormal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtGrdPacientes = new System.Windows.Forms.DataGridView();
            this.btnVolver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdPacientes)).BeginInit();
            this.SuspendLayout();
            // 
            // dtGrdPacientes
            // 
            this.dtGrdPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGrdPacientes.Location = new System.Drawing.Point(160, 80);
            this.dtGrdPacientes.Name = "dtGrdPacientes";
            this.dtGrdPacientes.Size = new System.Drawing.Size(240, 150);
            this.dtGrdPacientes.TabIndex = 0;
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(240, 271);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 1;
            this.btnVolver.Text = "volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // PacientesNormal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.dtGrdPacientes);
            this.Name = "PacientesNormal";
            this.Text = "PacientesNormal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PacientesNormal_FormClosed);
            this.Load += new System.EventHandler(this.PacientesNormal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdPacientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtGrdPacientes;
        private System.Windows.Forms.Button btnVolver;
    }
}