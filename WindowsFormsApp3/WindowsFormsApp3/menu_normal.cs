﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;

namespace WindowsFormsApp3
{
    public partial class menu_normal : Form
    {
        public menu_normal()
        {
            InitializeComponent();
        }

        private void btnTratamiento_Click(object sender, EventArgs e)
        {
            MostrarTabla cambio = new MostrarTabla("seleccionarTratamiento",2);
            cambio.Show();
            this.Hide();
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            MostrarTabla cambio = new MostrarTabla("seleccionarIngreso",3);
            cambio.Show();
            this.Hide();
        }

        private void btnDoctores_Click(object sender, EventArgs e)
        {
            MostrarTabla cambio = new MostrarTabla("seleccionarDoctores",4);
            cambio.Show();
            this.Hide();
        }

        private void btnPacientes_Click(object sender, EventArgs e)
        {
            MostrarTabla cambio = new MostrarTabla("seleccionarPacientes",1);
            cambio.Show();
            this.Hide();
        }

        private void btnEnfermeras_Click(object sender, EventArgs e)
        {
            MostrarTabla cambio = new MostrarTabla("seleccionarEnfermeras",5);
            cambio.Show();
            this.Hide();
        }
    }
}
